import java.io.*;
import java.util.*;


public class SDA18192T {
    //int
    SDA18192T() {
        try {
            BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
            String inp[] = br.readLine().split(" "); // untuk input permata awal dan batas kalah
            int banyakBola = Integer.parseInt(inp[0]); // ini jumlah awal
            int thresholdKalah = Integer.parseInt(inp[1]); //ini batas maksimal
            LinkedList list = new LinkedList();
            String warna[] = br.readLine().split(" ");
            int[] bola = new int[banyakBola];
            for (int i=0;i<banyakBola;i++ ) {
                int ball = Integer.parseInt(warna[i]);
                list.insertAtEnd(ball);
                // System.out.println(list.size);
            }
            // ini warna warna
            int perintah = Integer.parseInt(br.readLine());// ini jumlah perintah
            for (int i=0;i<list.size/3 ;i++ ) {
                list.checkMatch();
            }
            // System.out.println(list.size);

            for(int i=0;i<perintah;i++) {
                String tembak[] = br.readLine().split(" ");
                list.insertAtPos(Integer.parseInt(tembak[1]), Integer.parseInt(tembak[0]));
                list.checkMatch();
                // System.out.println(list.size);
                if (list.size < 4) {
                    System.out.println("MENANG");
                    break;
                }
                if (list.size>thresholdKalah) {
                    System.out.println("KALAH");
                    break;
                }
            }
            if (list.size>=4 && list.size<=thresholdKalah) {
                System.out.println(list.size);
            }
        }
        catch (Exception e) {
            ;
        }
    }
    public static void main(String[] args) {
        new SDA18192T();
    }
}
class Node {
    int element;
    Node next,prev;
    Node() {
        this.element = 0;
        this.next = this.prev = null;
    }
    Node(int data, Node next, Node prev) {
        this.element = data;
        this.next = next;
        this.prev = prev;
    }

}


class LinkedList {
    Node start;
    Node end ;
    int size;

    /* Constructor */
    public LinkedList() {
        start = null;
        end = null;
        size = 1;
    }
    /* Function to insert element at begining */
    public void insertAtStart(int val) {
        Node node = new Node(val, null, null);
        if (start==null) {
            node.next = node;
            node.prev = node;
            start = node;
            end = node;
        }
        else {
            node.next=start;
            node.prev = end;
            end.next = node;
            start.prev = node;
            start = node;
        }
        size++;
    }
    public void insertAtEnd(int val) {
        Node node = new Node(val, null, null);
        if (start==null) {
            node.next = node;
            node.prev = node;
            start = node;
            end = node;
            return;
        }
        node.prev = end;
        end.next= node;
        start.prev = node;
        node.next = start;
        end = node;
        ++size;
    }
    /* Function to insert element at position */
    public void insertAtPos(int val , int pos) {
        Node node = new Node(val, null, null);
        if (pos == 0) {
            insertAtStart(val);
            return;
        }
        Node ptr = start;
        for (int i = 1; i < size; i++) {
            if (i == pos) {
                Node tmp = ptr.next;
                ptr.next = node;
                node.prev = ptr;
                node.next = tmp;
                tmp.prev = node;
            }
            ptr = ptr.next;
        }
        size++ ;
    }
    /* Function to delete node at position  */
    public void deleteAtPos(int pos) {
        if (pos == 1) {
            if (size == 1) {
                start = null;
                end = null;
                size = 0;
                return;
            }
            start = start.next;
            start.prev=end;
            end.next=start;
            size--;
            return ;
        }
        if (pos == size) {
            end = end.prev;
            end.next = start;
            start.prev=end;
            size-- ;
        }
        Node ptr = start.next;
        for (int i = 2; i <= size; i++) {
            if (i == pos) {
                Node p = ptr.prev;
                Node n = ptr.next;

                p.next= n;
                n.prev =p;
                size-- ;
                return;
            }
            ptr = ptr.next;
        }
    }
    public void checkMatch() {
        /*
        flow nya -> ambil node sekarang, cek sama nextnya, kalo sama jadiin pivot, check kirinya, kalo sama counter nambah, kalo nggak yauds
        */
        Node flag1 = start;
        Node flag2 = start;
        int counter = 1;

        while(flag1.element == flag1.prev.element) {
            counter++;
            flag1 = flag1.prev;
        }
        if (counter==size) {
            start= null;
            end=null;
            size=0;
            return;
        }
        for (int i=1;i<size-counter ;i++ ) {
            if (flag2.element == flag2.next.element) {
                counter++;
                flag2= flag2.next;
            }
            else {
                if (counter<3) {
                    flag2=flag2.next;
                    flag1 = flag2;
                }
            }
        }
        if (counter<3) {
            return;
        }
        flag1.prev.next = flag2.next;
        flag2.next.prev = flag1.prev;
        this.size = size-counter;
    }


}
// source : https://www.sanfoundry.com/java-program-implement-circular-doubly-linked-list/
