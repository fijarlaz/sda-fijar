import java.util.*;
import java.io.*;

/**
 * TP3 SDA
 * @author Millenio Ramadizsa - 1706040063
 * Collaborator and reference: Muhammad Ridho Ananda - 1706028682
 * Reference: https://kukuruku.co/post/avl-trees/
 */

public class SDA1819T32 {
  static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

  public static void main(String[] args) {
    try {
      Abyss abyss = new Abyss();
      String reads[] = br.readLine().split(" ");
      for (int i = 0; i < Integer.parseInt(reads[0]); i++) {
        String reads2[] = br.readLine().split(" ");
        if (reads2[0].equals("GABUNG")) {
          if (reads2.length == 2) abyss.root = abyss.insert(abyss.root, reads2[1]);
          else abyss.root = abyss.insertEye(abyss.root, reads2[1], Integer.parseInt(reads2[2]));
        } else if (reads2[0].equals("PARTNER")) {
          Explorer partner = abyss.findPartner(reads2[1]);
          if (partner == null) System.out.println("TIDAK ADA");
          else System.out.println(partner.name);
        } else if (reads2[0].equals("MENYERAH")) {
          abyss.root = abyss.remove(abyss.root, reads2[1], true);
        } else if (reads2[0].equals("CEDERA")) {
          abyss.root = abyss.remove(abyss.root, reads2[1], false);
        } else if (reads2[0].equals("PRINT")) {
          abyss.print();
        } else if (reads2[0].equals("MUSIBAH")) {
          abyss.root = abyss.catastrophe(abyss.root);
          System.out.println();
        } else if (reads2[0].equals("PANJANG-TALI")) {
          System.out.println(abyss.findLength(reads2[1], reads2[2]));
        } else if (reads2[0].equals("GANTUNGAN")) {
          Explorer ancestor = null;
          for (int x = 2; x <= Integer.parseInt(reads2[1]); x++) {
            if (x == 2) ancestor = abyss.lca(abyss.root, reads2[x], reads2[x + 1]);
            else ancestor = abyss.lca(abyss.root, ancestor.name, reads2[x + 1]);
          }
          System.out.println(ancestor.name);
        }
      }
    } catch (IOException e) {
      System.out.println(e);
    }
  }
}

class Explorer {
  public String name;
  public Explorer left;
  public Explorer right;
  public int height;
  public int sub;

  public Explorer(String name) {
    this.name = name;
    this.left = null;
    this.right = null;
    this.height = 1;
    this.sub = 1;
  }
}

class Abyss {
  public Explorer root;

  public Abyss() {
    this.root = null;
  }

  public int height(Explorer explorer) {
    if (explorer == null) return 0;
    else return explorer.height;
  }

  public int subTree(Explorer explorer) {
    if (explorer == null) return 0;
    else return explorer.sub;
  }

  public int bfactor(Explorer explorer) {
    return height(explorer.right) - height(explorer.left);
  }

  public void fixHeight(Explorer explorer) {
    int heightLeft = height(explorer.left);
    int heightRight = height(explorer.right);
    if (heightLeft > heightRight) explorer.height = heightLeft + 1;
    else explorer.height = heightRight + 1;
    explorer.sub = subTree(explorer.left) + subTree(explorer.right) + 1;
  }

  public Explorer rotateRight(Explorer explorer) {
    Explorer temp = explorer.left;
    explorer.left = temp.right;
    temp.right = explorer;
    fixHeight(explorer);
    fixHeight(temp);
    return temp;
  }

  public Explorer rotateLeft(Explorer explorer) {
    Explorer temp = explorer.right;
    explorer.right = temp.left;
    temp.left = explorer;
    fixHeight(explorer);
    fixHeight(temp);
    return temp;
  }

  public Explorer balance(Explorer explorer) {
    fixHeight(explorer);
    if (bfactor(explorer) == 2) {
      if (bfactor(explorer.right) < 0) explorer.right = rotateRight(explorer.right);
      return rotateLeft(explorer);
    }
    if (bfactor(explorer) == -2) {
      if (bfactor(explorer.left) > 0) explorer.left = rotateLeft(explorer.left);
      return rotateRight(explorer);
    }
    return explorer;
  }

  public Explorer insert(Explorer explorer, String name) {
    if (explorer == null) return new Explorer(name);
    if (name.compareTo(explorer.name) < 0) explorer.left = insert(explorer.left, name);
    else explorer.right = insert(explorer.right, name);
    return balance(explorer);
  }

  public Explorer findMin(Explorer explorer) {
    if (explorer.left != null) return findMin(explorer.left);
    else return explorer;
  }

  public Explorer findMax(Explorer explorer) {
    if (explorer.right != null) return findMax(explorer.right);
    else return explorer;
  }

  public Explorer removeMin(Explorer explorer) {
    if (explorer.left == null) return explorer.right;
    explorer.left = removeMin(explorer.left);
    return balance(explorer);
  }

  public Explorer removeMax(Explorer explorer) {
    if (explorer.right == null) return explorer.left;
    explorer.right = removeMax(explorer.right);
    return balance(explorer);
  }

  public Explorer remove(Explorer explorer, String name, boolean isSurrender) {
    if (explorer == null) return null;
    if (name.compareTo(explorer.name) < 0) {
      explorer.left = remove(explorer.left, name, isSurrender);
    } else if (name.compareTo(explorer.name) > 0) {
      explorer.right = remove(explorer.right, name, isSurrender);
    } else {
      if (isSurrender == true) {
        return replaceLeft(explorer);
      } else {
        if (explorer.left == null && explorer.right == null) return replaceLeft(explorer);
        else if (explorer.left == null) return replaceRight(explorer);
        else if (explorer.right == null) return replaceLeft(explorer);
        Explorer maxLeft = findMax(explorer.left);
        Explorer minRight = findMin(explorer.right);
        int distanceLeft =  distance(explorer, maxLeft);
        int distanceRight = distance(explorer, minRight);
        if (distanceLeft > distanceRight) return replaceLeft(explorer);
        else return replaceRight(explorer);
      }
    }
    return balance(explorer);
  }

  public Explorer replaceRight(Explorer explorer) {
    Explorer exLeft = explorer.left;
    Explorer exRight = explorer.right;
    if (exRight == null) return exLeft;
    Explorer min = findMin(exRight);
    min.right = removeMin(exRight);
    min.left = exLeft;
    return balance(min);
  }

  public Explorer replaceLeft(Explorer explorer) {
    Explorer exLeft = explorer.left;
    Explorer exRight = explorer.right;
    if (exLeft == null) return exRight;
    Explorer max = findMax(exLeft);
    max.left = removeMax(exLeft);
    max.right = exRight;
    return balance(max);
  }

  public int distance(Explorer explorer, Explorer target) {
    if (explorer == target) return 0;
    else if (target == null) return 0;
    else if (target.name.compareTo(explorer.name) > 0) return distance(explorer.right, target) + 1;
    else return distance(explorer.left, target) + 1;
  }

  public void preOrder(Explorer explorer) {
    if (explorer == null) return;
    System.out.print(explorer.name + ";");
    preOrder(explorer.left);
    preOrder(explorer.right);
  }

  public void postOrder(Explorer explorer) {
    if (explorer == null) return;
    postOrder(explorer.left);
    postOrder(explorer.right);
    System.out.print(explorer.name + ";");
  }

  public Explorer catastrophe(Explorer explorer) {
    if (explorer.left == null && explorer.right == null) {
      System.out.print(explorer.name + ";");
      return null;
    } else {
      if (explorer.left != null) explorer.left = catastrophe(explorer.left);
      if (explorer.right != null) explorer.right = catastrophe(explorer.right);
      return balance(explorer);
    }
  }

  public Explorer findParent(Explorer explorer, Explorer parent, Explorer find) {
    if (explorer == find) {
      return parent;
    } else {
      if (find.name.compareTo(explorer.name) > 0) return findParent(explorer.right, explorer, find);
      else return findParent(explorer.left, explorer, find);
    }
  }

  public Explorer find(Explorer explorer, String name) {
    if (explorer == null || explorer.name.equals(name)) return explorer;
    if (explorer.name.compareTo(name) > 0) return find(explorer.left, name);
    return find(explorer.right, name);
  }

  public Explorer findPartner(String name) {
    Explorer current = find(root, name);
    if (current == root) return null;
    Explorer parent = findParent(root, null, current);
    if (parent.left == current) return parent.right;
    return parent.left;
  }

  public void print() {
    preOrder(root);
    System.out.println();
    postOrder(root);
    System.out.println();
  }

  public Explorer lca(Explorer explorer, String first, String second) {
    if (explorer == null) return null;
    if (explorer.name.compareTo(first) > 0 && explorer.name.compareTo(second) > 0)
      return lca(explorer.left, first, second);
    if (explorer.name.compareTo(first) < 0 && explorer.name.compareTo(second) < 0)
      return lca(explorer.right, first, second);
    return explorer;
  }

  public int findLength(String first, String second) {
    Explorer ancestor = lca(root, first, second);
    Explorer exFirst = find(root, first);
    Explorer exSec = find(root, second);
    int distanceFirst =  distance(ancestor, exFirst);
    int distanceSec = distance(ancestor, exSec);
    int total = distanceFirst + distanceSec;
    return total;
  }

  public Explorer insertEye(Explorer explorer, String name, int view) {
    if (explorer == null) return insert(explorer, name);
    int rootPosition = subTree(explorer.left);
    int position = findPosition(explorer, name);
    if (position > rootPosition) {
      if (position - rootPosition <= view) return insert(explorer, name);
    } else {
      if ((rootPosition + 1) - position <= view) return insert(explorer, name);
    }
    return explorer;
  }

  public int findPosition(Explorer explorer, String name) {
    if (explorer == null) return 0;
    if (name.compareTo(explorer.name) < 0) return findPosition(explorer.left, name);
    else return subTree(explorer.left) + findPosition(explorer.right, name) + 1;
  }
}
