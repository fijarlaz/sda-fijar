Outline editorial:
1. AVL Tree:
  https://kukuruku.co/post/avl-trees/
  https://geeksforgeeks.org/avl-tree-set-1-insertion/
  https://www.geeksforgeeks.org/avl-tree-set-2-deletion/

  itu template avltree, paling engga, kalian udah bisa insert biasa + delete biasa (untuk delete yang di soal(cedera / menyerah) tinggal diedit2)

2. Lowest Common Ancestor:
  https://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/

  LCA = dari dua node, kita mau tau 2 node yang menjadi atasannya yang sama yang terakhir.
  dipake di :
    query gantungan (murni LCA)
    query ntar kita dari LCA yang kita temuin, kita mau cari jarak total dari (A - (LCA(A, B)) - B)
3. Query Partner (memanfaatkan method find yang biasanya udah ada di template-template)
  kita tinggal cari parent dari node yang ditanyakan
  misal kita disuruh cari partnernya A, kita cari aja parentnya A, kalo udah, kita cek darisitu, A itu anak kiri apa kanannya, kalo udah nemu, ntar kita tinggal tentuin partnernya itu siapa :)
4. Query insert String K
  ini sedikit lebih mikir dari yang sebelumnya, mungkin kalo LWT bakalan dibahas.



--> masukin potongan kode dibawah ini biar bisa debug cantik
1. ganti aja n.key dengan n.(nama key kalian)
2. ganti n.lChild dengan n.(nama left child kalian)
3. ganti n.rChild dengan n.(nama r child kalian)
 // public void printDebug() {
   //     // System.out.println("SDFDG");
   //     List<List<String>> lines = new ArrayList<List<String>>();


   //     List<Node> level = new ArrayList<Node>();
   //     List<Node> next = new ArrayList<Node>();

   //     level.add(this.root);
   //     int nn = 1;

   //     int widest = 0;

   //     while (nn != 0) {
   //       List<String> line = new ArrayList<String>();

   //       nn = 0;

   //       for (Node n : level) {
   //         if (n == null) {
   //           line.add(null);

   //           next.add(null);
   //           next.add(null);
   //         } else {
   //           String aa = n.name;
   //           line.add(aa);
   //           if (aa.length() > widest) widest = aa.length();

   //           next.add(n.left);
   //           next.add(n.right);

   //           if (n.left != null) nn++;
   //           if (n.right != null) nn++;
   //         }
   //       }

   //       if (widest % 2 == 1) widest++;

   //       lines.add(line);

   //       List<Node> tmp = level;
   //       level = next;
   //       next = tmp;
   //       next.clear();
   //     }

   //     int perpiece = lines.get(lines.size() - 1).size() * (widest + 4);
   //     for (int i = 0; i < lines.size(); i++) {
   //       List<String> line = lines.get(i);
   //       int hpw = (int) Math.floor(perpiece / 2f) - 1;

   //       if (i > 0) {
   //         for (int j = 0; j < line.size(); j++) {

   //                 // split node
   //           char c = ' ';
   //           if (j % 2 == 1) {
   //             if (line.get(j - 1) != null) {
   //               c = (line.get(j) != null) ? '┴' : '┘';
   //             } else {
   //                 if (j < line.size() && line.get(j) != null) c = '└';
   //             }
   //           }
   //           System.out.print(c);

   //                 // lines and spaces
   //           if (line.get(j) == null) {
   //             for (int k = 0; k < perpiece - 1; k++) {
   //               System.out.print(" ");
   //             }
   //           } else {

   //             for (int k = 0; k < hpw; k++) {
   //               System.out.print(j % 2 == 0 ? " " : "─");
   //             }
   //               System.out.print(j % 2 == 0 ? "┌" : "┐");
   //             for (int k = 0; k < hpw; k++) {
   //               System.out.print(j % 2 == 0 ? "─" : " ");
   //             }
   //           }
   //         }
   //         System.out.println();
   //       }

   //         // print line of numbers
   //       for (int j = 0; j < line.size(); j++) {

   //         String f = line.get(j);
   //         if (f == null) f = "";
   //         int gap1 = (int) Math.ceil(perpiece / 2f - f.length() / 2f);
   //         int gap2 = (int) Math.floor(perpiece / 2f - f.length() / 2f);

   //             // a number
   //         for (int k = 0; k < gap1; k++) {
   //           System.out.print(" ");
   //         }
   //         System.out.print(f);
   //         for (int k = 0; k < gap2; k++) {
   //           System.out.print(" ");
   //         }
   //       }
   //       System.out.println();

   //       perpiece /= 2;
   //     }
   // }



public void printDebug() {
       // System.out.println("SDFDG");
       List<List<String>> lines = new ArrayList<List<String>>();


       List<Node> level = new ArrayList<Node>();
       List<Node> next = new ArrayList<Node>();

       level.add(this.root);
       int nn = 1;

       int widest = 0;

       while (nn != 0) {
         List<String> line = new ArrayList<String>();

         nn = 0;

         for (Node n : level) {
           if (n == null) {
             line.add(null);

             next.add(null);
             next.add(null);
           } else {
             String aa = n.name;
             line.add(aa);
             if (aa.length() > widest) widest = aa.length();

             next.add(n.left);
             next.add(n.right);

             if (n.left != null) nn++;
             if (n.right != null) nn++;
           }
         }

         if (widest % 2 == 1) widest++;

         lines.add(line);

         List<Node> tmp = level;
         level = next;
         next = tmp;
         next.clear();
       }

       int perpiece = lines.get(lines.size() - 1).size() * (widest + 4);
       for (int i = 0; i < lines.size(); i++) {
         List<String> line = lines.get(i);
         int hpw = (int) Math.floor(perpiece / 2f) - 1;

         if (i > 0) {
           for (int j = 0; j < line.size(); j++) {

                   // split node
             char c = ' ';
             if (j % 2 == 1) {
               if (line.get(j - 1) != null) {
                 c = (line.get(j) != null) ? '┴' : '┘';
               } else {
                   if (j < line.size() && line.get(j) != null) c = '└';
               }
             }
             System.out.print(c);

                   // lines and spaces
             if (line.get(j) == null) {
               for (int k = 0; k < perpiece - 1; k++) {
                 System.out.print(" ");
               }
             } else {

               for (int k = 0; k < hpw; k++) {
                 System.out.print(j % 2 == 0 ? " " : "─");
               }
                 System.out.print(j % 2 == 0 ? "┌" : "┐");
               for (int k = 0; k < hpw; k++) {
                 System.out.print(j % 2 == 0 ? "─" : " ");
               }
             }
           }
           System.out.println();
         }

           // print line of numbers
         for (int j = 0; j < line.size(); j++) {

           String f = line.get(j);
           if (f == null) f = "";
           int gap1 = (int) Math.ceil(perpiece / 2f - f.length() / 2f);
           int gap2 = (int) Math.floor(perpiece / 2f - f.length() / 2f);

               // a number
           for (int k = 0; k < gap1; k++) {
             System.out.print(" ");
           }
           System.out.print(f);
           for (int k = 0; k < gap2; k++) {
             System.out.print(" ");
           }
         }
         System.out.println();

         perpiece /= 2;
       }
   }