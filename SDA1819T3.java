import java.io.*;
import java.util.*;

public class SDA1819T3 {
    SDA1819T3() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            int order = Integer.parseInt(br.readLine());
//            System.out.println(order-1);
            Tree tree = new Tree();
            for (int i=0;i<order ;i++ ) {
                //@TODO masukin input here
                String perintah[] = br.readLine().split(" ");
                if (perintah[0].equals("GABUNG")) {
                    //@TODO add method
                    if (perintah.length ==3) {
                        Node orang = new Node(perintah[1]);
                        int jarPan = Integer.parseInt(perintah[2]);
                        tree.Gabung(orang, jarPan);
                    }
                    else {
                        Node orang = new Node(perintah[1]);
//                        System.out.println(orang.name);
                        tree.root = tree.Gabung(tree.root, orang);
                    }
                }
                else if (perintah[0].equals("PARTNER")) {
                    //@TODO add method
                    Node orang = new Node(perintah[1]);
                    tree.Partner(orang);
                }
                else if (perintah[0].equals("MENYERAH")) {
                    //@TODO add method
                    Node orang = new Node(perintah[1]);
                    tree.Menyerah(orang);
                }
                else if (perintah[0].equals("CEDERA")) {
                    //@TODO add method
                    Node orang = new Node(perintah[1]);
                    tree.Cedera(orang);
                }
                else if (perintah[0].equals("PRINT")) {
                    //@TODO add method
                    tree.printAll();
                }
                else if (perintah[0].equals("MUSIBAH")) {
                    //@TODO add method
                    tree.printMusibah();

                }
                else if (perintah[0].equals("PANJANG-TALI")) {
                    //@TODO add method
                    Node orang1 = new Node(perintah[1]);
                    Node orang2 = new Node(perintah[2]);
                    tree.panjangTali(orang1, orang2);
                }
                else if (perintah[0].equals("GANTUNGAN")) {

                }
                // tree.printDebug();
            }
        }
        catch (Exception e){
            ;
        }
    }

    public static void main(String[] args) {
        new SDA1819T3();
    }
}

class Node {
    String name;
    Node left;
    Node right;
    int height;
    int branch;

    public Node(String name) {
        this.name = name;
        this.left = null;
        this.right = null;
        this.height = 1;
        this.branch = 1;
    }
}

class Tree{
    Node root;

    public Tree() {
        this.root = null;
    }

    public int max(int a, int b) {
        if (a>b) {
            return a;
        }
        return b;
    }

    public int height(Node node) {
        if (node == null) {
            return 0;           
        }
        return node.height;
    }

    public int getBranch(Node node) {
        if (node == null) {
            return 0;
        }
        return node.branch;
    }

    public int getLevel(Node node) {
        return getLevel(this.root, node, 0);
    }
    public int getLevel(Node parent, Node node, int currentLevel) {
        if (parent == null) return -1;
        if (parent.compareTo(node)==0) return currentLevel;
        int left =getLevel(parent.left, node, currentLevel+1);
        if (left== -1) return getLevel(parent.right, node, currentLevel+1);
        return left;
    }

    public int heightDiff(Node node) {
        return height(node.right) - height(node.left);
    }

    public void theHeight(Node node) {
        int leftChild = height(node.left);
        int rightChild = height(node.right);
        node.height = max(leftChild, rightChild)+1;
        node.branch = getBranch(node.left) + getBranch(node.right) +1;
    }

    public int balance(Node node) {
        if (node==null) {
            return 0;
        }
        return getHeight(node.left) - getHeight(node.right);
    }

    public Node getMinNode(Node node) {
        Node current = node;
        while(current.left != null) {
            current = current.left;
        }
        return current;
    }

    public Node getMaxNode(Node node) {
        Node current = node;
        while(current.right != null) {
            current = current.right;
        }
        return current;
    }

    public Node rmMinNode(Node node) {
        if (node.left == null) {
            return node.right;
        }
        node.left = rmMinNode(node.left);
        return checkBalance(node);
    }

    public Node rmMaxNode(Node node) {
        if (node.right == null) {
            return node.left;
        }
        node.right = rmMaxNode(node.right);
        return checkBalance(node);
    }

    public Node rightRotation(Node node) {
        // Node x = node.left;
        // Node temp = x.right;

        // x.right = node;
        // node.left = temp;

        // node.height = max(getHeight(node.left), getHeight(node.right)) +1;
        // x.height = max(getHeight(x.left), getHeight(x.right)) +1;

        // return x;

        Node temp = node.left;
        node.left = temp.right;
        temp.right = node;
        theHeight(node);
        theHeight(temp);
        return temp;
    }

    public Node leftRotation(Node node) {
        // Node x = node.right;
        // Node temp = x.left;

        // x.left = node;
        // node.right = temp;

        // node.height = max(getHeight(node.left), getHeight(node.right)) +1;
        // x.height = max(getHeight(x.left), getHeight(x.right)) +1;

        // return x;

        Node temp = node.right;
        node.right = temp.left;
        temp.left = node;
        theHeight(node);
        theHeight(temp);
        return temp;
    }



    public Node Gabung(Node node) {
//        System.out.println(node.name + "test");

       return Gabung(this.root, node);
    }

    public void Gabung(Node node, int jarPan) {
        Gabung(root, node, jarPan);
    }

    public Node Gabung(Node parent, Node node) {
        if (parent == null) {
            return node;
        }
        if (node.name.compareTo(parent.name) < 0) {
            parent.left = Gabung(parent.left, node);
        }
        else {
            parent.right = Gabung(parent.right, node);
        }
        return checkBalance(parent);
    }



    public Node Partner(Node person) {
        return person;
    }


    public void Menyerah(Node node) {
        this.root = Menyerah(this.root, node);
    }

    public Node Menyerah(Node parent, Node node) {
        //@TODO
        if (parent == null) return parent;

        if (node.compareTo(parent) < 0) {
            parent.left = Menyerah(parent.left, node);
        }

        else if (node.compareTo(parent) > 0) {
            parent.right = Menyerah(parent.right, node);
        }

        else {

            if ((parent.left == null) || (parent.right == null)) {
                Node temp = null;
                if (temp == parent.left) temp = parent.right;
                else temp = parent.left;
                if (temp == null) {
                    temp = parent;
                    parent = null;
                }
                else {
                    parent = temp;
                }
            }
            else {
                Node temp = getMaxNode(parent.left);

                parent.name = temp.name;

                parent.left = Menyerah(parent.left, temp);
            }
        }
        if (parent == null) return parent;

        parent.height = max(getHeight(parent.left), getHeight(parent.right)) +1;

        checkBalance(parent, node);

        return parent;
    }

    public Node deleteNode(Node parent, Node node) {
        return node;
    }

    public void Cedera(Node node) {
        this.root = Cedera(this.root, node);
    }

    public Node Cedera(Node parent, Node node) {
        if (parent == null) {
            return parent;
        }

        if (node.compareTo(parent) < 0) {
            parent.left = Cedera(parent.left, node);
        }
        else if (node.compareTo(parent) > 0) {
            parent.right = Cedera(parent.right, node);
        }

        else {
            if ((parent.left == null)|| (parent.right == null)) {
                Node temp = null;
                if (temp == parent. left) temp = parent.right;
                else temp = parent.left;

                if (temp == null) {
                    temp = parent;
                    parent = null;
                }
                else {
                    parent = temp;
                }
            }
            else {
                Node temp = getMinNode(parent.right);

                parent.name = temp.name;

                parent.right = Cedera(parent.right, temp);
            }
        }
        if (parent == null) return parent;

        parent.height = max(getHeight(parent.left), getHeight(parent.right)) +1;

        checkBalance(parent, node);

        return parent;
    }

    public Node checkBalance(Node node) {
        theHeight(node);
        if (heightDiff(node)>1) {
            if (heightDiff(node.right) < 0) {
                node.right = rightRotation(node.right);
            }
            return leftRotation(node);
        }
        if (heightDiff(node) < -1) {
            if (heightDiff(node.left) >0 ) {
                node.left = leftRotation(node.left);
            }
            return rightRotation(node);
        }
        return node;
    }

//    public void reBalance(Node node) {
//        int diff = getHeight(node.left) - getHeight(node.right);
//        if (diff > 1) {
//            if (getHeight(node.left.left) > getHeight(node.left.right)) {
//                node = rightRotation(node);
//            }
//            else{
//                node = leftRotation(node);
//            }
//        }
//        else if(diff <-1) {
//            if (getHeight(node.right.left) > getHeight(node.right.left)) {
//
//            }
//        }
//    }

    public void printAll() {
        printPreOrder(this.root);
        System.out.println();
        printPostOrder(this.root);
        System.out.println();
    }

    public void printPreOrder(Node node) {
        if (node == null) return;

        System.out.print(node.name+";");
        printPreOrder(node.left);
        printPreOrder(node.right);
    }

    public void printPostOrder(Node node) {
        if (node == null) return;
        printPreOrder(node.left);
        printPreOrder(node.right);
        System.out.print(node.name+";");
    }

    public void printMusibah() {
        printMusibah(this.root);
    }

    public void printMusibah(Node position) {
        if (position == null) {
            return;
        }
        if (position.left!= null) {
            printMusibah(position.left);
        }
        if (position.right!=null) {
            printMusibah(position.right);
        }
        System.out.print(position.name +";");
    }

    public int panjangTali(Node person1, Node person2) {
        return 1;
    }

    public void Gantungan() {
        //@TODO


    }

    
   
}

