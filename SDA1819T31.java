import java.io.*;
import java.util.*;

public class SDA1819T3 {
    SDA1819T3() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String order[] = br.readLine().split(" ");
            Tree tree = new Tree();
            for (int i=0;i<order.length ;i++ ) {
                //@TODO masukin input here
                String perintah[] = br.readLine().split(" ");
                if (perintah[0].equals("GABUNG")) {
                    //@TODO add method
                    if (perintah.length ==3) {
                        Node orang = new Node(perintah[1]);
                        int jarPan = Integer.parseInt(perintah[2]);
                        tree.Gabung(orang, jarPan);
                    }
                    else {
                        Node orang = new Node(perintah[1]);
                        tree.Gabung(orang);
                    }
                }
                else if (perintah[0].equals("PARTNER")) {
                    //@TODO add method
                    Node orang = new Node(perintah[1]);
                    tree.Partner(orang);
                }
                else if (perintah[0].equals("MENYERAH")) {
                    //@TODO add method
                    Node orang = new Node(perintah[1]);
                    tree.Menyerah(orang);
                }
                else if (perintah[0].equals("CEDERA")) {
                    //@TODO add method
                    Node orang = new Node(perintah[1]);
                    tree.Cedera(orang);
                }
                else if (perintah[0].equals("PRINT")) {
                    //@TODO add method
                    tree.printAll();
                }
                else if (perintah[0].equals("MUSIBAH")) {
                    //@TODO add method
                    tree.printMusibah();

                }
                else if (perintah[0].equals("PANJANG-TALI")) {
                    //@TODO add method
                    Node orang1 = new Node(perintah[1]);
                    Node orang2 = new Node(perintah[2]);
                    tree.panjangTali(orang1, orang2);
                }
                else if (perintah[0].equals("GANTUNGAN")) {

                }
            }
        }
        catch (Exception e){
            ;
        }
    }

    public static void main(String[] args) {
        new SDA1819T3();
    }
}

class Node implements Comparable<Node> {
    Node left;
    Node right;
    Node parent;
    int height;
    Node partner;
    String name;
    boolean dead;

    Node(String name) {
        this.name = name;
        this.partner = null;
        this.left = null;
        this.right = null;
        this.dead = false;
        this.height = 1;
    }

    public boolean isDead() {
        if (dead) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Node otherNode) {
        return this.name.compareTo(otherNode.name);
    }
}

class Tree{
    Node root;
    int penjelajah;

    public Tree() {
        this.root = null;
        this.penjelajah = 0;
    }

    public int max(int a, int b) {
        if (a>b) {
            return a;
        }
        return b;
    }

    public int getHeight(Node node) {
        if (node==null) {
            return 0;
        }
        return node.height;
    }

    public Node rightRotation(Node node) {
        Node x = node.left;
        Node temp = x.right;

        x.right = node;
        node.left = temp;

        node.height = max(getHeight(node.left), getHeight(node.right)) +1;
        x.height = max(getHeight(x.left), getHeight(x.right)) +1;

        return x;
    }

    public Node leftRotation(Node node) {
        Node x = node.right;
        Node temp = x.left;

        x.left = node;
        node.right = temp;

        node.height = max(getHeight(node.left), getHeight(node.right)) +1;
        x.height = max(getHeight(x.left), getHeight(x.right)) +1;

        return x;
    }

    public void Gabung(Node node) {
        Gabung(root, node);
    }

    public void Gabung(Node node, int jarPan) {
        Gabung(root, node, jarPan);
    }

    public void Gabung(Node parent, Node node) {
        if (node.compareTo(parent) > 0) {
            if (parent.right == null) {
                parent.right = node;
                node.parent = parent;
                node.partner = parent.left;
                penjelajah++;
                parent.height = max(getHeight(parent.left), getHeight(parent.right)) +1;
            }
            else {
                Gabung(parent.right, node);
            }
        }
        else {
            if (parent.left == null) {
                parent.left = node;
                node.parent = parent;
                node.partner = parent.right;
                penjelajah++;
                parent.height = max(getHeight(parent.left), getHeight(parent.right)) +1;
            }
            else {
                Gabung(parent.left, node);
            }
        }
        checkBalance(node);
    }

    public void Gabung(Node parent, Node node, int jarPan) {

    }

    public Node Partner(Node person) {
        return person.partner;
    }

    public void Menyerah(Node person) {
        //@TODO

        
    }



    public void Cedera(Node person) {

        
    }

    public void checkBalance(Node node) {
        int diff = getHeight(node.left) - getHeight(node.right);
        if (diff >1 || diff<-1) {
            reBalance(node);
        }
        if (node.parent == null) {
            return;
        }
        checkBalance(node);
    }

    public void reBalance(Node node) {
        int diff = getHeight(node.left) - getHeight(node.right);
        if (diff > 1) {
            if (getHeight(node.left.left) > getHeight(node.left.right)) {
                node = rightRotation(node);
            }
            else{
                node = leftRotation(node);
            }
        }
    }

    public void printAll() {
        printPostOrder();
        System.out.println();
        printPreOrder();
    }

    public void printPreOrder() {

    }

    public void printPostOrder() {

    }

    public void printMusibah() {
        printMusibah(this.root);
    }

    public void printMusibah(Node position) {
        if (position == null) {
            return;
        }
        System.out.print(position.name +";");
        printMusibah(position.left);
        printMusibah(position.right);
    }

    public int panjangTali(Node person1, Node person2) {
        return 1;
    }

    public void Gantungan() {
        //@TODO
    }

}